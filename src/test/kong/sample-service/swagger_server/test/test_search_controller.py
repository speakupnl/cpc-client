# coding: utf-8

from __future__ import absolute_import

from swagger_server.models.subscriber import Subscriber
from . import BaseTestCase
from six import BytesIO
from flask import json


class TestSearchController(BaseTestCase):
    """ SearchController integration test stubs """

    def test_get_subscriber_for_phonenumber(self):
        """
        Test case for get_subscriber_for_phonenumber

        subscriber information for this telephone number
        """
        response = self.client.open('/subscribers/v1//{PhoneNumber}'.format(PhoneNumber='PhoneNumber_example'),
                                    method='GET')
        self.assert200(response, "Response body is : " + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
