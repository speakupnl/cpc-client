#!/bin/bash
docker run -v $PWD:/config \
	--link kong \
	xebia/kongfig:latest \
		--host kong:8001 \
		--path /config/kongfig.yml
