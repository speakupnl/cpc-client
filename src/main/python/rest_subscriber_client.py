import os
import time
import hmac
import time
import hashlib
import argparse
from base64 import b64decode, b64encode
import requests
from random import randint
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import dsa, rsa
from cryptography.hazmat.primitives.serialization import load_pem_private_key
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import padding
from jwt import PyJWT
from datetime import datetime
from colorama import Fore 
from colorama import Style
import csv
import re


class RESTSubscriberClient(object):

    def __init__(self,host_url,api_dest,issuer,verbose):
        self.verbose = verbose
        self.jwt = PyJWT()
        self.issuer = issuer
        self.expires_after = 24 * 60 * 60
        self.not_before = 30
        self.load_private_key()
        self.load_shared_key(self.private_key)
        self.generate_token()
        self.host_url = host_url.rstrip('/')
        self.api_dest = api_dest
        self.headers = {}

    def load_private_key(self, filename='private-key.pem'):
        with open(filename, 'rb') as f:
            private_key_pem = f.read()
        self.private_key = load_pem_private_key(
            private_key_pem, password=None, backend=default_backend())
        if self.verbose:
            print('using private key: {}'.format(self.private_key))

    def load_shared_key(self, private_key,
                        filename='sharedkey.encrypted'):
        encrypted_key = None
        with open(filename, 'rb') as f:
            encrypted_key = b64decode(f.read())
        self.shared_key = private_key.decrypt(
            encrypted_key, padding.PKCS1v15())
        if self.verbose:
            print('using shared key: {}'.format(self.shared_key.decode('ascii')))

    def generate_token(self):
        self.now = int(time.mktime(datetime.now().timetuple()))
        self.expires_at = self.now + self.expires_after
        message = {
            'iss': self.issuer,
            'nbf': self.now - self.not_before,
            'exp': self.now + self.expires_after
        }
        token = self.jwt.encode(message, self.private_key, 'RS256')
        self.token = token.decode('ascii')

    def token_has_expired(self):
        now = int(time.mktime(datetime.now().timetuple()))
        return self.token is None or now + 5 > self.now
           
    def get_token(self):
        if self.token_has_expired():
            self.generate_token()
        return self.token

    def httpdate(self):
        """Return a string representation of a date according to RFC 1123
        (HTTP/1.1).

        The supplied date must be in UTC.
        """ 
        now=datetime.utcnow()
        weekday = [
            'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'][
            now.weekday()]
        month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep',
                 'Oct', 'Nov', 'Dec'][now.month - 1]
        return '{}, {:02d} {} {:04d} {:02d}:{:02d}:{:02d} GMT'.format(
            weekday, now.day, month, now.year, now.hour, now.minute, now.second)

    def add_hmac(self, request_line):
        x_date = self.httpdate()
        message = bytearray('date: {}\n{}'.format(x_date, request_line), 'ascii')

        digest = hmac.new(self.shared_key, msg=message,
                          digestmod=hashlib.sha256).digest()
        authorization = 'hmac username="%s", algorithm="hmac-sha256", headers="date request-line", signature="%s"' % (
            self.issuer, b64encode(digest).decode())

        self.headers['date'] = x_date
        self.headers['authorization'] = str(authorization)

    def get_subscriber(self,phone_number):
        self.headers = {}
        self.cookies = {'jwt': '%s' % self.get_token()}
        if self.api_dest == "v2":
            uri = '/subscribers/v2/%s' % phone_number
        else:
            uri = '/subscribers/v1/%s' % phone_number
        url = '%s%s' % (self.host_url, uri)
        self.add_hmac('GET %s HTTP/1.1' % uri)

        if self.verbose:
            print(Fore.RED + Style.BRIGHT + 'Generated headers for REST call:')
            print(Fore.YELLOW + ' {}'.format(self.headers))
            print(Style.RESET_ALL)
            print(Fore.RED + Style.BRIGHT + "CURL statement used for REST call:\n")
            print(Fore.YELLOW + "curl --cookie jwt={} --header 'authorization: {}' --header 'date: {}' {}".format(
                self.cookies['jwt'], self.headers['authorization'], self.headers['date'], url))
            print(Style.RESET_ALL)

        response = requests.get(
            url, cookies=self.cookies, headers=self.headers)
        print(Fore.RED + Style.BRIGHT + 'Response from server:' + Fore.GREEN +' [%s]' % response.status_code + Fore.RESET + Style.RESET_ALL)
        print(response.text)
#        if self.verbose:
#            print("curl --cookie jwt={} --header 'authorization: {}' --header 'date: {}' {}".format(
#                self.cookies['jwt'], self.headers['authorization'], self.headers['date'], url))

if __name__=='__main__':
    parser = argparse.ArgumentParser(description='run the script')
    parser.add_argument("--consumer-name", "-c", dest="issuer", required=True,
                        help="to use")
    parser.add_argument("--api", "-a", dest="api_dest", choices=['v1','v2'], default='v1',
                        help="to call v1/v2 allowed")
    parser.add_argument("--hostname", "-H", dest="host_url", required=True,
                        help="to call")
    parser.add_argument("--verbose", "-v", action="store_true",
                        dest="verbose", help="verbose mode")
    parser.add_argument("phone_number", 
                        help="to query")
    options = parser.parse_args()

    client = RESTSubscriberClient(options.host_url,options.api_dest,options.issuer,options.verbose)
    client.get_subscriber(options.phone_number)

