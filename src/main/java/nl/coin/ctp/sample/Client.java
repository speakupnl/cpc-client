package nl.coin.ctp.sample;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.*;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.*;
import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * sample client for invoking COIN APIs
 *
 * the successfully authenticate, the caller should authenticate it self through
 * a JSON Web Token (JWT) and  by providing a HMAC authenticaiton
 * by signing the date header and request-line
 * with the caller's shared secret key.
 *
 */
public class Client {
    /** consumer name in jwt token iss and hmac */
    private String user = null;

    /** to sign the JWT token */
    KeyPair keyPair = null;

    /** HMAC shared secret */
    private String hmacUser = null;
    private String secret = null;

    /** period in which the JWT token is valid */
    private int validPeriodInSeconds = 300;

    /**
     * Constructor.
     */
    public Client() {
    }

    public Client(String name, String privateKeyFile, String encryptedSharedKeyFile) {
        this.user = name;
        this.keyPair = readPrivateKey(privateKeyFile);
        this.hmacUser = name;
        this.secret = readSharedKey(encryptedSharedKeyFile, keyPair.getPrivate());
    }

    /**
     * reads the RSA key pair from disk
     *
     * @param filename containing the private key
     * @return the KeyPair contained.
     */
    public KeyPair readPrivateKey(String filename) {
        try (InputStream is = new FileInputStream(filename)) {

            if (Security.getProvider("BC") == null) {
                Security.addProvider(new BouncyCastleProvider());
            }
            PEMParser parser = new PEMParser(new InputStreamReader(is));
            PEMKeyPair pemKeyPair = (PEMKeyPair) parser.readObject();
            KeyPair keyPair = new JcaPEMKeyConverter()
                    .setProvider("BC")
                    .getKeyPair(pemKeyPair);
            return keyPair;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /** decrypts the encrypted sharedkey from `filename`  using `privateKey`
     */
    public String readSharedKey(String filename, PrivateKey privateKey) {

        try {
            String s = FileUtils.readFileToString(new File(filename) ,"ascii").trim();
            byte[] encryptedSharedKey = Base64.getDecoder().decode(s);
            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            cipher.init(Cipher.DECRYPT_MODE, privateKey);
            return new String(cipher.doFinal(encryptedSharedKey), "ascii");
        } catch (NoSuchAlgorithmException|InvalidKeyException|NoSuchPaddingException|IOException|BadPaddingException|IllegalBlockSizeException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * adds the JWT token as Bearer token in the Authorization header.
     * the token is signed with the private key and contains both the
     * issuer (iss), expiration time (exp) and not-before (nbf) time claims.
     */
    public void addJwtAuthentication(HttpRequestBase request) {
        Calendar cal = Calendar.getInstance();
        Algorithm algorithm = Algorithm.RSA256(
                (RSAPublicKey) keyPair.getPublic(), (RSAPrivateKey) keyPair.getPrivate());


        cal.add(Calendar.SECOND, -30);
        Date nbf = cal.getTime();

        cal.add(Calendar.SECOND, 30 + validPeriodInSeconds);
        Date exp = cal.getTime();

        String token = JWT.create()
                .withIssuer(user)
                .withExpiresAt(exp)
                .withNotBefore(nbf)
                .sign(algorithm);
        request.addHeader("Cookie",
                String.format("jwt=%s; path=/;domain=%s", token, request.getURI().getHost()));
        System.out.println(token);
    }

    /**
     * The date in HMAC digest should be in RFC1123 format
     */
    private String getCurrentDateInHttpHeaderFormat() {
        DateFormat rfc1123 = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss 'GMT'");
        rfc1123.setTimeZone(TimeZone.getTimeZone("UTC"));
        return rfc1123.format(new Date().getTime());
    }

    /**
     * alculates the HMAC digest using the current 'date' header and the HTTP request-line.
     *
     * the message over which the digest is calculate is specified at
     * https://getkong.org/plugins/hmac-authentication/.
     *
     */
    private String calculateHttpRequestHmac(HttpRequest request) {
        HmacSha256Signer signer = new HmacSha256Signer(secret);
        final String HMAC_HEADER_FORMAT = "hmac username=\"%s\", algorithm=\"hmac-sha256\", headers=\"date request-line\", signature=\"%s\"";

        // Remove the protocol://<host> prefix from the request line. That is not passed to the server
        String requestLine = request.getRequestLine().toString();
        Pattern requestLinePattern = Pattern.compile("^([A-Z][A-Z]* )[^:]*://[^/]*", Pattern.DOTALL);
        Matcher matcher = requestLinePattern.matcher(requestLine);
        if (matcher.find()) {
            requestLine = matcher.replaceAll(matcher.group(1));
        }

        // message format to digest: see https://getkong.org/plugins/hmac-authentication/
        String message = String.format("%s\n%s",
                request.getFirstHeader("date"), requestLine);
        String signature = signer.sign(message);
        String result = String.format(HMAC_HEADER_FORMAT, hmacUser, signature);

        System.out.println(message);
        System.out.println(signature);
        System.out.println(result);

        return result;
    }


    /**
     * adds date of the request and the HMAC digest in the Authorization header.
     */
    public void addHmacAuthentication(HttpRequestBase request) {
        request.addHeader("date", getCurrentDateInHttpHeaderFormat());
        request.addHeader("authorization", calculateHttpRequestHmac(request));
    }

    /**
     * calls the abonnee service
     */
    public void callService(int expectedStatus) {

        HttpClient httpClient = HttpClients.createDefault();

        try {
            HttpGet request = new HttpGet("http://localhost:8000/subscribers/v1/1000000000");
            addJwtAuthentication(request);
            addHmacAuthentication(request);

            HttpResponse response;
            response = httpClient.execute(request);
            StatusLine status = response.getStatusLine();
            String body = EntityUtils.toString(response.getEntity());

            if (status.getStatusCode() != expectedStatus) {
                System.err.println(String.format("Expected status %d, got %d\n", expectedStatus, status.getStatusCode()));
            } else {
                System.err.println(String.format("service returned %d", expectedStatus));
            }

            System.out.println(body);
        } catch (IOException e) {
            System.err.println("failed to call the service, " + e.getMessage());
            throw new RuntimeException(e);
        }
    }

    /**
     * runs the client. One call is with a valid JWT token. the second call with an expired token.
     */
    public void run() throws Exception {
        callService(HttpStatus.SC_OK);
        validPeriodInSeconds = -3600;
        callService(HttpStatus.SC_UNAUTHORIZED);
    }

    /**
     * run client
     */
    public static void main(String[] args) throws Exception {
        Client c = null;
        if (args.length == 3) {
            c = new Client(args[0], args[1], args[2]);
        } else if (args.length == 0){
            c = new Client("sample-user", "private-key.pem",  "sharedkey.encrypted");
        } else {
            System.out.println("Usage: Client username privatekey-file sharedkey-filename");
            System.exit(1);
        }
        c.run();
    }
}
