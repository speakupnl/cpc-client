package nl.coin.ctp.sample;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.Base64;
import java.util.Formatter;

/**
 * HMAC SHA1 signer
 */
public class HmacSha256Signer {
    private static final String HMAC_SHA_ALGORITHM = "HMacSHA256";
    private Mac signer;

    public HmacSha256Signer(String key) {
        try {
            SecretKey sk = new SecretKeySpec(key.getBytes("UTF-8"), HMAC_SHA_ALGORITHM);
            signer = Mac.getInstance(HMAC_SHA_ALGORITHM);
            signer.init(sk);
        } catch (UnsupportedEncodingException|NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException(e);
        }
    }

    public String sign(String data) {
        try {
            signer.reset();
            byte[] signature = signer.doFinal(data.getBytes("UTF-8"));
            return new String(Base64.getEncoder().encode(signature));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }
}
